﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

    public Transform[] shields;
    public float rotationSpeed = 1000f;
    public float shieldDistance = 0.2f;
    public Transform target;

    private GameSettings settings;

	// Use this for initialization
	void Start () {
        settings = GameController.instance.settings;
        float angle;
        Vector2 pos;
        for (var i = 0; i < shields.Length; i++)
        {
            angle = Mathf.PI * 2f * i / shields.Length;
            pos = (0.5f * settings.sphereSize + shieldDistance) * new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
            shields[i].localPosition = pos;
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
        transform.position = target.position;
	}
}
