﻿using UnityEngine;
using System.Collections;

public class Puck : MonoBehaviour
{
    public GameSettings settings;

    private Rigidbody2D body;

    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        GameController.instance.puckBody = transform;
    }

    // Update is called once per frame
    void Update()
    {
        //Friction
        if (body == null) return;
        var speed = body.velocity.magnitude;
        if (speed < settings.puckDeceleration * Time.deltaTime) body.velocity = Vector2.zero;
        else body.velocity *= (speed - settings.puckDeceleration * Time.deltaTime) / speed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.collider.tag);
        switch (collision.collider.tag)
        {
            case "Player":
                Bounce(GameController.instance.currentPlayerVelocity, collision.collider.transform.position);
                break;
            case "Enemy":
                var enemyVelocity = GameController.instance.currentEnemyVelocity.normalized * settings.maxVelocity;
                if (enemyVelocity == Vector2.zero) enemyVelocity = (transform.position - collision.collider.transform.position).normalized * settings.maxVelocity;
                Bounce(enemyVelocity, collision.collider.transform.position);
                break;
            case "Border":
                //var velocity = body.velocity;
                //velocity.x *= -1f;
                //body.velocity = velocity;
                break;
            case "Base":
                GameController.instance.HitBase(transform.position.y < 0f, GameController.instance.settings.puckDamage, true);
                GameController.instance.puckBody = null;
                Destroy(gameObject);
                break;
            default:
                break;
        }
    }

    //private void OnTriggerStay2D(Collider2D collision)
    //{
    //    switch (collision.tag)
    //    {
    //        case "Player":
    //            //MoveFromSphere(collision.transform.position);
    //            break;
    //        case "Enemy":
    //            //MoveFromSphere(collision.transform.position);
    //            break;
    //        case "Border":
    //            var velocity = body.velocity;
    //            if (velocity.x > 0 == transform.position.x > 0) velocity.x *= -1f;
    //            body.velocity = velocity;
    //            break;
    //        case "Base":
    //            break;
    //        default:
    //            break;
    //    }
    //}

    private void Bounce(Vector2 cueVelocity, Vector3 cuePosition)
    {
        var relativeVelocity = body.velocity - cueVelocity;
        Vector2 dir = (cuePosition - transform.position).normalized;
        var normalComponent = dir * Vector2.Dot(body.velocity, dir);
        var tangentComponent = body.velocity - normalComponent;
        var relativeComponent = dir * Vector2.Dot(relativeVelocity, dir);
        relativeComponent *= -settings.puckBounciness;
        body.velocity = (tangentComponent + relativeComponent).normalized*settings.maxVelocity;
        Debug.Log(body.velocity);
        MoveFromSphere(cuePosition);
    }

    private void MoveFromSphere(Vector3 cuePos)
    {
        var dir = transform.position - cuePos;
        transform.position = cuePos + dir.normalized * 0.5f * (settings.sphereSize + settings.puckSize);
    }
}
