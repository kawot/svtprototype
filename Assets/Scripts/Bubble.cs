﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour {

    public float bubbleSize = 0.3f;
    public Transform target;
    public int livesLeft = 0;

    private GameSettings settings;

	// Use this for initialization
	void Start () {
        settings = GameController.instance.settings;
        transform.localScale = (settings.sphereSize + bubbleSize) * Vector3.one;
        livesLeft = GameController.instance.bonusSettings.bubbleLives;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = target.position;
	}

    public void Hit()
    {
        livesLeft--;
        if (livesLeft <= 0) Destroy(gameObject);
    }
}
