﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "AIConfig", menuName = "Settings/AI", order = 1)]
public class AISettings : ScriptableObject
{
    public float maxVelocity = 5f;
    [Range(0f,1f)] public float smoothing = 1f;
    public float loadDelay = 2f;
    public AINavType type = AINavType.Closest;
}

[System.Serializable]
public enum AINavType { Closest, All }