﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BonusConfig", menuName = "Settings/Bonus", order = 1)]
public class BonusSettings : ScriptableObject
{
    public float startDelay = 4f;
    public float nextDelay = 4f;
    public bool inOrder = false;
    public List<Bonus> bonusList;
    [Space(10)]
    [Header("Special bonus parameters")]
    [Range(0f, 1f)] public float tarSpeedPercentage = 0.5f;
    [Range(1f, 3f)] public float swiftnessSpeedPercentage = 2f;
    public float vacuumSpeedIncreasePercentage = 0.5f;
    public float regenerationRate = 4f;
    public GameObject shadowPrefab;
    public float shadowLifetime = 1f;
    public GameObject healingSalvePrefab;
    public float healingSalveLifetime = 1f;
    public int healingSalveCount = 5;
    public int healingSalveRestoreAmount = 5;
    [Range(0f, 80f)] public float splitShotAngle = 30f;
    public float miniUziSpeedIncreasePercentage = 0.5f;
    [Range(0.1f, 1f)] public float miniUziSizePercentage = 0.4f;
    public int fireballBonusDamage = 2;
    public GameObject shieldsPrefab;
    public GameObject minesPrefab;
    public int minesCount = 4;
    public int minesDamage = 5;
    public GameObject bubblePrefab;
    public int bubbleLives = 5;
    [Range(0.1f, 1f)] public float freezeSpeedReduction = 0.25f;
    public float freezeSpeedRestore = 0.25f;
    public float freezeStopTime = 2f;
}

[System.Serializable]
public struct Bonus
{
    public BonusType type;
    public float chanceWeight;
    public float time;
    public Color color;
}

[System.Serializable]
public enum BonusType
{
    None,
    Tar,
    Swiftness,
    Vacuum,
    Regeneration,
    Dwarfishness,
    Puck,
    Shadow,
    Healing,
    Spikes,
    Turrets,
    SplitShot,
    MiniUzi,
    Sneak,
    Shields,
    Bubble,
    Mines,
    FreezeBalls,
    FireBalls
}