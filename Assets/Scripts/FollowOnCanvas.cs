﻿using UnityEngine;
using System.Collections;

public class FollowOnCanvas : MonoBehaviour
{
    public Transform target;
    private Camera cam;

    // Use this for initialization
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null) return;
        var targetPos = cam.WorldToScreenPoint(target.position);
        transform.position = targetPos;
    }

    public void Init (Transform target)
    {
        this.target = target;
    }
}
