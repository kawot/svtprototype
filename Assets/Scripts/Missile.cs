﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

    private Rigidbody2D body;
    public Color definedColor = Color.clear;

	// Use this for initialization
	void Start () {
        tag = transform.position.y < 0f ? "Player" : "Enemy";
        if (definedColor == Color.clear) GetComponent<SpriteRenderer>().color = transform.position.y < 0f ? Color.green : Color.red;
        else GetComponent<SpriteRenderer>().color = definedColor;
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.tag)
        {
            case "Player":
                if (tag == "Enemy")
                {
                    GameController.ShrinkPlayer();
                    Destroy(gameObject);
                }
                break;
            case "Enemy":
                if (tag == "Player")
                {
                    GameController.instance.playerMissiles.Remove(transform);
                    GameController.ShrinkEnemy();
                    Destroy(gameObject);
                }
                break;
            case "Border":
                var velocity = body.velocity;
                velocity.x *= -1f;
                body.velocity = velocity;
                break;
            case "Base":
                GameController.instance.HitBase(transform.position.y < 0f);
                if (transform.position.y > 0f) GameController.instance.playerMissiles.Remove(transform);
                Destroy(gameObject);
                break;
            case "Shield":
                if (tag == "Player" && transform.position.y > 0f)
                {
                    GameController.instance.playerMissiles.Remove(transform);
                    Destroy(gameObject);
                    var bubble = collision.GetComponent<Bubble>();
                    if (bubble != null) bubble.Hit();
                }
                if (tag == "Enemy" && transform.position.y < 0f)
                {
                    Destroy(gameObject);
                    var bubble = collision.GetComponent<Bubble>();
                    if (bubble != null) bubble.Hit();
                }
                break;
            default:
                Debug.Log(collision.tag);
                break;
        }
    }
}
