﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour {

    public float f = 10f;

    private Color baseColor;
    private SpriteRenderer renderer;
    private float offset;

	// Use this for initialization
	void Start () {
        renderer = GetComponent<SpriteRenderer>();
        baseColor = renderer.color;
        offset = Random.Range(0f, Mathf.PI * 2f);
	}
	
	// Update is called once per frame
	void Update () {
        var x = Time.time * f + offset;
        var y = Mathf.Sin(x);
        renderer.color = Color.Lerp(baseColor, Color.red, y);
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameController.instance.HitBase(transform.position.y < 0f, GameController.instance.bonusSettings.minesDamage);
        Destroy(gameObject);
    }
}
