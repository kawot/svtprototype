﻿using UnityEngine;
using System.Collections;

public class Salve : MonoBehaviour
{
    public float rotateSpeed = -1000f;
    public float shrinkRate = 1f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back * rotateSpeed);
        transform.localScale *= (1f - Time.deltaTime * shrinkRate);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameController.instance.HealBase(transform.position.y < 0f);
        Destroy(gameObject);
    }


}
