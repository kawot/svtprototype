﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public GameSettings settings;
    public AISettings AIsettings;
    public BonusSettings bonusSettings;
    public GameObject borderPrefab;
    public GameObject basePrefab;
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject missilePrefab;
    public GameObject puckPrefab;
    public GameObject bonusTriggerObject;
    public Image bonusDisplay;
    public Transform gameField;
    public Text timerLabel;
    public Text playerBaseLabel;
    public Text enemyBaseLabel;
    public Text playerBulletLabel;
    public Text enemyBulletLabel;
    public Text puckTimerLabel;

    private int playerLives;
    private int enemyLives;
    private bool playing = false;
    private bool playerIsShooting = false;
    private bool enemyIsShooting = false;
    private float playerBullets = 0;
    private float enemyBullets = 0;
    private Camera cam;
    private Transform playerBody;
    private Transform enemyBody;
    [HideInInspector] public Transform puckBody;
    [HideInInspector] public Vector2 currentEnemyVelocity = Vector2.zero;
    [HideInInspector] public Vector2 currentPlayerVelocity = Vector2.zero;
    [HideInInspector] public List<Transform> playerMissiles = new List<Transform>();
    private Bonus currentBonus = new Bonus();
    private int bonusIndex = -1;
    private float bonusTimer;
    private bool bonusIsActive = false;
    private bool bonusOwnerIsPlayer = true;
    private float bonusCounter = 0f; 

    public static void ShrinkPlayer()
    {
        instance.ShrinkCue(instance.playerBody);
    }

    public static void ShrinkEnemy()
    {
        instance.ShrinkCue(instance.enemyBody);
    }

    public void HitBase(bool our, int damage = 1, bool byPuck = false)
    {
        if (bonusIsActive && currentBonus.type == BonusType.FireBalls && (our != bonusOwnerIsPlayer))
        {
            damage += bonusSettings.fireballBonusDamage;
        }
        if (our)
        {
            playerLives -= damage;
            playerBaseLabel.text = playerLives.ToString();
        }
        else
        {
            enemyLives -= damage;
            enemyBaseLabel.text = enemyLives.ToString();
        }
        if (byPuck) StartCoroutine(DeployPuck(settings.puckRespawnDelay));
    }

    public void HealBase(bool our)
    {
        var amount = bonusSettings.healingSalveRestoreAmount;
        if (our)
        {
            playerLives += amount;
            if (playerLives > settings.lives) playerLives = settings.lives;
            playerBaseLabel.text = playerLives.ToString();
        }
        else
        {
            enemyLives += amount;
            if (enemyLives > settings.lives) enemyLives = settings.lives;
            enemyBaseLabel.text = enemyLives.ToString();
        }
    }

    public void ActivateBonus(bool byPlayer = true)
    {
        bonusOwnerIsPlayer = byPlayer;
        var c = currentBonus.color;
        c.a = 0.2f;
        bonusDisplay.color = c;
        bonusTriggerObject.SetActive(false);
        bonusIsActive = true;
        //bonusTimer = Mathf.Max(currentBonus.time, bonusSettings.nextDelay);
        bonusTimer = currentBonus.time + bonusSettings.nextDelay;
        if (currentBonus.type == BonusType.Shadow)
        {
            playerBody.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.2f);
            enemyBody.GetComponent<SpriteRenderer>().color = Color.clear;
            c = enemyBulletLabel.color;
            c.a = 0f;
            enemyBulletLabel.color = c;
        }
        else if (currentBonus.type == BonusType.Sneak && bonusOwnerIsPlayer)
        {
            playerBody.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.2f);
        }
        else if (currentBonus.type == BonusType.Sneak && !bonusOwnerIsPlayer)
        {
            enemyBody.GetComponent<SpriteRenderer>().color = Color.clear;
            c = enemyBulletLabel.color;
            c.a = 0f;
            enemyBulletLabel.color = c;
        }
        else if (currentBonus.type == BonusType.Healing)
        {
            StartCoroutine(DeployHealingSalves(currentBonus.time, bonusSettings.healingSalveCount));
        }
        else if (currentBonus.type == BonusType.Shields)
        {
            var parentBody = bonusOwnerIsPlayer ? playerBody : enemyBody;
            GameObject guards = Instantiate(bonusSettings.shieldsPrefab, parentBody.position, Quaternion.identity, gameField);
            guards.GetComponent<Shield>().target = parentBody;
            Destroy(guards, currentBonus.time);
        }
        else if(currentBonus.type == BonusType.Mines)
        {
            Vector2 pos;
            for (var i = 0; i < bonusSettings.minesCount; i++)
            {
                if (bonusOwnerIsPlayer)
                {
                    pos = cam.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), Random.Range(0.6f, 0.9f)));
                }
                else
                {
                    pos = cam.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), Random.Range(0.1f, 0.4f)));
                }
                GameObject mine = Instantiate(bonusSettings.minesPrefab, pos, Quaternion.identity, gameField);
                Destroy(mine, currentBonus.time);
            }
        }
        else if (currentBonus.type == BonusType.Bubble)
        {
            var parentBody = bonusOwnerIsPlayer ? playerBody : enemyBody;
            var b = FindObjectOfType<Bubble>();
            if (b != null) Destroy(b.gameObject);
            GameObject bubble = Instantiate(bonusSettings.bubblePrefab, parentBody.position, Quaternion.identity, gameField);
            bubble.GetComponent<Bubble>().target = parentBody;
        }
        else if (currentBonus.type == BonusType.FreezeBalls)
        {
            bonusCounter = 1f;
        }
    }

	// Use this for initialization
	void Start ()
    {
        instance = this;
        cam = Camera.main;
        SetField();
        playerBulletLabel.GetComponent<FollowOnCanvas>().Init(playerBody);
        enemyBulletLabel.GetComponent<FollowOnCanvas>().Init(enemyBody);
        playerBulletLabel.text = "Hold to move";
        enemyBulletLabel.text = "Show me your best, meatbag";
        puckTimerLabel.text = "";
        enemyBullets = settings.startingBullets - AIsettings.loadDelay * settings.bulletLoadFrequency;
        playerBullets = settings.startingBullets;
        StartCoroutine(StartTimer());
        bonusTriggerObject.SetActive(false);
        bonusTimer = bonusSettings.startDelay;
        bonusDisplay.color = Color.clear;
        bonusIndex = Random.Range(0, bonusSettings.bonusList.Count);
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(1)) SpawnRandomBonus();

        var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0f;

        if (Input.GetMouseButtonDown(0) && mousePos.y > 0f)
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(0);
        }

        //if (Input.GetMouseButtonDown(0) && Vector3.Distance(playerBody.position, mousePos) < settings.sphereSize)
        //    playerIsShooting = false;
        //if (Input.GetMouseButtonUp(0))
        //{
        //    playerIsShooting = true;
        //    StartCoroutine(PlayerShooting());
        //}

        if (playerIsShooting)
        {

        }
        else
        {
            //Load
            if (!playing) return;
            playerBullets += Time.deltaTime * settings.bulletLoadFrequency;
            if (playerBullets > settings.bulletCapacity)
            {
                playerBullets = settings.bulletCapacity;
                playerIsShooting = true;
                StartCoroutine(PlayerShooting());
            }
            playerBulletLabel.text = Mathf.FloorToInt(playerBullets).ToString();
        }

        //Move
        if (mousePos.y > -0.1f) mousePos.y = -0.1f;
        if (mousePos.y < settings.baseHight - cam.orthographicSize) mousePos.y = settings.baseHight - cam.orthographicSize;
        var dir = mousePos - playerBody.position;
        var deltaMove = dir.normalized * settings.maxVelocity * Time.deltaTime;
        if (bonusIsActive && currentBonus.type == BonusType.Tar)
            deltaMove *= bonusSettings.tarSpeedPercentage;
        else if (bonusIsActive && currentBonus.type == BonusType.Swiftness)
            deltaMove *= bonusSettings.swiftnessSpeedPercentage;
        else if (bonusIsActive && currentBonus.type == BonusType.FreezeBalls && !bonusOwnerIsPlayer)
            deltaMove *= bonusCounter;
        if (deltaMove.magnitude > dir.magnitude) deltaMove = dir;
        playerBody.position += deltaMove;
        currentPlayerVelocity = dir.normalized * settings.maxVelocity;

        //Resize
        RestoreCue(playerBody);
        RestoreCue(enemyBody);

        if (!playing) return;
        if (playerLives <= 0 || enemyLives <= 0)
        {
            Time.timeScale = 0f;
            playing = false;
        }
        RunAI();
        RunBonuses();
    }

    private void RunBonuses()
    {
        if (bonusIsActive && currentBonus.type == BonusType.FreezeBalls)
        {
            if (bonusCounter > 0f)
            {
                bonusCounter += bonusSettings.freezeSpeedRestore * Time.deltaTime;
                if (bonusCounter > 1f) bonusCounter = 1f;
            }
        }

        if (bonusIsActive) currentBonus.time -= Time.deltaTime;
        bonusTimer -= Time.deltaTime;
        if (bonusTimer < 0f)
        {
            bonusTimer = bonusSettings.nextDelay;
            SpawnRandomBonus();
        }
        else if (currentBonus.time < 0f)
        {
            bonusIsActive = false;
            bonusDisplay.color = Color.clear;
            if (currentBonus.type == BonusType.Shadow || currentBonus.type == BonusType.Sneak || currentBonus.type == BonusType.FreezeBalls)
            {
                playerBody.GetComponent<SpriteRenderer>().color = Color.white;
                enemyBody.GetComponent<SpriteRenderer>().color = Color.white;
                var c = enemyBulletLabel.color;
                c.a = 1f;
                enemyBulletLabel.color = c;
            }
        }
    }

    private void RunAI()
    {
        if (enemyIsShooting)
        {
            
        }
        else
        {
            //Load AI
            enemyBullets += Time.deltaTime * settings.bulletLoadFrequency;
            if (enemyBullets > 0f) enemyBulletLabel.text = Mathf.FloorToInt(enemyBullets).ToString();
            if (enemyBullets > settings.bulletCapacity)
            {
                enemyBullets = settings.bulletCapacity;
                enemyIsShooting = true;
                StartCoroutine(EnemyShooting());
            }
        }
        //Move AI
        var targetPos = enemyBody.position;
        if (puckBody != null)
        {
            if (puckBody.position.y + 0.5f * Mathf.Max(settings.puckSize, settings.sphereSize) > enemyBody.position.y)
            {
                targetPos = enemyBody.position + Vector3.up;
            }
            else
            {
                targetPos = puckBody.position + Vector3.up * 0.5f * Mathf.Max(settings.puckSize, settings.sphereSize);
            }
        }
        else if (playerMissiles.Count == 0) { targetPos = playerBody.position; targetPos.y *= -1f; }//target positon can be set to the center of enemy side here  
        else switch (AIsettings.type)
            {
                case AINavType.Closest:
                    var closestDist = Vector2.Distance(enemyBody.position, playerMissiles[0].position);
                    var closestMissile = playerMissiles[0];
                    for (var i = 1; i < playerMissiles.Count; i++)
                    {
                        var dist = Vector2.Distance(enemyBody.position, playerMissiles[i].position);
                        if (dist < closestDist)
                        {
                            closestDist = dist;
                            closestMissile = playerMissiles[i];
                        }
                    }
                    targetPos = closestMissile.position;
                    break;
                case AINavType.All:
                    for (var i = 0; i < playerMissiles.Count; i++)
                    {
                        var vecToMissile = playerMissiles[i].position - enemyBody.position;
                        var dir = vecToMissile.normalized;
                        var dist = vecToMissile.magnitude;
                        targetPos += dir / dist;
                    }
                    break;
                default:
                    break;
            }
        if (targetPos.y < 0.1f) targetPos.y = 0.1f;
        var moveDir = targetPos - enemyBody.position;
        var targetVelocity = moveDir.normalized * AIsettings.maxVelocity;
        var velocity = Vector3.Lerp(targetVelocity, currentEnemyVelocity, AIsettings.smoothing);
        if (enemyBody.position.y < 0.1f && velocity.y < 0f) velocity.y = 0f;
        currentEnemyVelocity = velocity;
        if (bonusIsActive && currentBonus.type == BonusType.Tar)
            velocity *= bonusSettings.tarSpeedPercentage;
        else if (bonusIsActive && currentBonus.type == BonusType.Swiftness)
            velocity *= bonusSettings.swiftnessSpeedPercentage;
        else if (bonusIsActive && currentBonus.type == BonusType.FreezeBalls && bonusOwnerIsPlayer)
            velocity *= bonusCounter;
        enemyBody.position += velocity * Time.deltaTime;
    }

    private void SetField()
    {
        var pos = new Vector3(0f, 0.5f, 0f);
        pos = cam.ViewportToWorldPoint(pos);
        pos.z = 0f;
        var size = new Vector3(settings.borderSize, cam.orthographicSize * 2f, 1f);
        GameObject leftBorder = Instantiate(borderPrefab, pos, Quaternion.identity, gameField);
        leftBorder.transform.localScale = size;
        pos.x *= -1f;
        GameObject rightBorder = Instantiate(borderPrefab, pos, Quaternion.identity, gameField);
        rightBorder.transform.localScale = size;

        pos = Vector3.up * cam.orthographicSize;
        size = new Vector3(10f, settings.baseHight, 1f);
        GameObject topBase = Instantiate(basePrefab, pos, Quaternion.identity, gameField);
        topBase.transform.localScale = size;
        pos.y *= -1f;
        GameObject bottomBase = Instantiate(basePrefab, pos, Quaternion.identity, gameField);
        bottomBase.transform.localScale = size;
        playerLives = settings.lives;
        enemyLives = settings.lives;
        playerBaseLabel.text = playerLives.ToString();
        enemyBaseLabel.text = enemyLives.ToString();

        pos.y *= 0.5f;
        GameObject player = Instantiate(playerPrefab, pos, Quaternion.identity, gameField);
        player.transform.localScale = Vector3.one * settings.sphereSize;
        pos.y *= -1f;
        playerBody = player.transform;
        GameObject enemy = Instantiate(enemyPrefab, pos, Quaternion.identity, gameField);
        enemy.transform.localScale = Vector3.one * settings.sphereSize;
        enemyBody = enemy.transform;
    }

    //private void SpawnMissiles()
    //{
    //    var angle = Random.Range(-1f, 1f) * settings.missileAngle * Mathf.Deg2Rad;
    //    var velocity = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0f) * settings.missileVelocity;
    //    GameObject missile = Instantiate(missilePrefab, playerBody.position, Quaternion.identity, gameField);
    //    missile.GetComponent<Rigidbody2D>().velocity = velocity;
    //    missile.transform.localScale = Vector3.one * settings.missileSize;
    //    playerMissiles.Add(missile.transform);

    //    angle = Random.Range(-1f, 1f) * settings.missileAngle * Mathf.Deg2Rad;
    //    velocity = new Vector3(Mathf.Sin(angle), -Mathf.Cos(angle), 0f) * settings.missileVelocity;
    //    missile = Instantiate(missilePrefab, enemyBody.position, Quaternion.identity, gameField);
    //    missile.GetComponent<Rigidbody2D>().velocity = velocity;
    //    missile.transform.localScale = Vector3.one * settings.missileSize;

    //}

    private void SpawnPlayerMissile()
    {
        var angle = Random.Range(-1f, 1f) * settings.missileAngle * Mathf.Deg2Rad;
        if (bonusIsActive && currentBonus.type == BonusType.SplitShot && bonusOwnerIsPlayer)
        {
            angle = bonusSettings.splitShotAngle * bonusCounter * Mathf.Deg2Rad;
            bonusCounter++;
            if (bonusCounter > 1) bonusCounter = -1;
        }
        var velocity = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0f) * settings.missileVelocity;
        if (bonusIsActive && currentBonus.type == BonusType.Vacuum)
        {
            velocity *= (1f + bonusSettings.vacuumSpeedIncreasePercentage);
        }
        GameObject missile = Instantiate(missilePrefab, playerBody.position, Quaternion.identity, gameField);
        missile.transform.localScale = Vector3.one * settings.missileSize;
        if (bonusIsActive && currentBonus.type == BonusType.MiniUzi && bonusOwnerIsPlayer)
        {
            velocity *= (1f + bonusSettings.miniUziSpeedIncreasePercentage);
            missile.transform.localScale *= bonusSettings.miniUziSizePercentage;
        }
        missile.GetComponent<Rigidbody2D>().velocity = velocity;
        playerMissiles.Add(missile.transform);
        if (bonusIsActive && (currentBonus.type == BonusType.Shadow || (currentBonus.type == BonusType.Sneak && bonusOwnerIsPlayer)))
        {
            GameObject shadow = Instantiate(bonusSettings.shadowPrefab, playerBody.position, Quaternion.identity, gameField);
            shadow.transform.localScale = Vector3.one * playerBody.localScale.x;
            Destroy(shadow, bonusSettings.shadowLifetime);
        }
        if (bonusIsActive && (currentBonus.type == BonusType.FireBalls || currentBonus.type == BonusType.FreezeBalls) && bonusOwnerIsPlayer)
        {
            missile.GetComponent<Missile>().definedColor = currentBonus.color;
        }
    }

    private void SpawnEnemyMissile()
    {
        var angle = Random.Range(-1f, 1f) * settings.missileAngle * Mathf.Deg2Rad;
        if (bonusIsActive && currentBonus.type == BonusType.SplitShot && !bonusOwnerIsPlayer)
        {
            angle = bonusSettings.splitShotAngle * bonusCounter * Mathf.Deg2Rad;
            bonusCounter++;
            if (bonusCounter > 1) bonusCounter = -1;
        }
        var velocity = new Vector3(Mathf.Sin(angle), -Mathf.Cos(angle), 0f) * settings.missileVelocity;
        if (bonusIsActive && currentBonus.type == BonusType.Vacuum)
        {
            velocity *= (1f + bonusSettings.vacuumSpeedIncreasePercentage);
        }
        GameObject missile = Instantiate(missilePrefab, enemyBody.position, Quaternion.identity, gameField);
        missile.transform.localScale = Vector3.one * settings.missileSize;
        if (bonusIsActive && currentBonus.type == BonusType.MiniUzi && !bonusOwnerIsPlayer)
        {
            velocity *= (1f + bonusSettings.miniUziSpeedIncreasePercentage);
            missile.transform.localScale *= bonusSettings.miniUziSizePercentage;
        }
        missile.GetComponent<Rigidbody2D>().velocity = velocity;
        if (bonusIsActive && (currentBonus.type == BonusType.Shadow || (currentBonus.type == BonusType.Sneak && !bonusOwnerIsPlayer)))
        {
            GameObject shadow = Instantiate(bonusSettings.shadowPrefab, enemyBody.position, Quaternion.identity, gameField);
            shadow.transform.localScale = Vector3.one * enemyBody.localScale.x;
            Destroy(shadow, bonusSettings.shadowLifetime);
        }
        if (bonusIsActive && (currentBonus.type == BonusType.FireBalls || currentBonus.type == BonusType.FreezeBalls) && !bonusOwnerIsPlayer)
        {
            missile.GetComponent<Missile>().definedColor = currentBonus.color;
        }
    }

    private void SpawnPuck()
    {
        GameObject puck = Instantiate(puckPrefab, gameField);
        puck.transform.localScale = settings.puckSize * Vector3.one;
        puck.GetComponent<Puck>().settings = settings;
    }

    private void ShrinkCue(Transform cue)
    {
        var size = cue.localScale.x;
        size = (size - settings.sphereMinSize) * (1f - settings.sphereSizeReduction) + settings.sphereMinSize;
        cue.localScale = Vector3.one * size;
        if (bonusIsActive && (currentBonus.type == BonusType.Shadow || (currentBonus.type == BonusType.Sneak && bonusOwnerIsPlayer == (cue == playerBody))))
        {
            GameObject shadow = Instantiate(bonusSettings.shadowPrefab, cue.position, Quaternion.identity, gameField);
            shadow.transform.localScale = Vector3.one * size;
            Destroy(shadow, bonusSettings.shadowLifetime);
        }
        else if(bonusIsActive && currentBonus.type == BonusType.FreezeBalls && bonusOwnerIsPlayer == (cue != playerBody))
        {
            if (bonusCounter <= 0f) return;
            bonusCounter -= bonusSettings.freezeSpeedReduction;
            if (bonusCounter <= 0f)
            {
                bonusCounter = 0f;
                cue.GetComponent<SpriteRenderer>().color = currentBonus.color;
                currentBonus.time = bonusSettings.freezeStopTime;
            } 
        }
    }

    private void RestoreCue(Transform cue)
    {
        var size = cue.localScale.x;
        if (bonusIsActive && currentBonus.type == BonusType.Dwarfishness)
        {
            size = settings.sphereMinSize;
        }
        else
        {
            var restoreAmount = settings.sphereSizeRestore;
            if (bonusIsActive && currentBonus.type == BonusType.Regeneration)
                restoreAmount *= bonusSettings.regenerationRate;
            size += restoreAmount * Time.deltaTime;
            if (size > settings.sphereSize) size = settings.sphereSize;
        }
        cue.localScale = Vector3.one * size;
    }

    private void SpawnRandomBonus()
    {
        if (bonusSettings.inOrder)
        {
            bonusIndex++;
            if (bonusIndex >= bonusSettings.bonusList.Count) bonusIndex = 0;
            currentBonus = bonusSettings.bonusList[bonusIndex];
        }
        else
        {
            float sum = 0f;
            foreach (Bonus b in bonusSettings.bonusList)
            {
                sum += b.chanceWeight;
            }
            if (sum == 0f) { Debug.LogError("sum of weights = 0"); return; }
            var rnd = Random.Range(0f, sum);
            for (var i = 0; i < bonusSettings.bonusList.Count; i++)
            {
                rnd -= bonusSettings.bonusList[i].chanceWeight;
                if (rnd <= 0f)
                {
                    currentBonus = bonusSettings.bonusList[i];
                    break;
                }
            }
        }
        SpawnBonus();
    }

    private void SpawnBonus()
    {
        var pos = cam.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), 0.5f));
        pos.z = 0f;
        bonusTriggerObject.transform.position = pos;
        bonusTriggerObject.SetActive(true);
        bonusTriggerObject.GetComponent<SpriteRenderer>().color = currentBonus.color;
    }

    IEnumerator StartTimer()
    {
        var timer = settings.startTimer;
        while(timer > 0)
        {
            timerLabel.text = timer.ToString();
            yield return new WaitForSeconds(1);
            timer--;
        }
        timerLabel.text = "";
        playing = true;
        StartCoroutine(DeployPuck(settings.puckDelay));
    }

    IEnumerator PlayerShooting()
    {
        while (settings.missileFrequency != 0f && playerIsShooting && playerBullets>=1f)
        {
            var delay = 1f / settings.missileFrequency;
            SpawnPlayerMissile();
            playerBullets -= 1f;
            playerBulletLabel.text = Mathf.FloorToInt(playerBullets).ToString();
            yield return new WaitForSeconds(delay);
        }
        playerIsShooting = false;
    }

    IEnumerator EnemyShooting()
    {
        while (settings.missileFrequency != 0f && enemyIsShooting && enemyBullets >= 1f)
        {
            var delay = 1f / settings.missileFrequency;
            SpawnEnemyMissile();
            enemyBullets -= 1f;
            enemyBulletLabel.text = Mathf.FloorToInt(enemyBullets).ToString();
            yield return new WaitForSeconds(delay);
        }
        enemyIsShooting = false;
    }

    IEnumerator DeployPuck(float delay)
    {
        var remains = delay % 1f;
        var wholeSeconds = Mathf.CeilToInt(delay);
        yield return new WaitForSeconds(remains);
        for (var i = wholeSeconds; i > 0 ; i--)
        {
            //puckTimerLabel.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        puckTimerLabel.text = "";
        SpawnPuck();
    }

    IEnumerator DeployHealingSalves(float time, int count)
    {
        var period = time / count;
        while (count > 0)
        {
            var pos = cam.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), Random.Range(0.1f, 0.4f)));
            pos.z = 0f;
            GameObject salveP = Instantiate(bonusSettings.healingSalvePrefab, pos, Quaternion.identity, gameField);
            Destroy(salveP, bonusSettings.healingSalveLifetime);
            pos = cam.ViewportToWorldPoint(new Vector3(Random.Range(0.1f, 0.9f), Random.Range(0.6f, 0.9f)));
            pos.z = 0f;
            GameObject salveE = Instantiate(bonusSettings.healingSalvePrefab, pos, Quaternion.identity, gameField);
            Destroy(salveE, bonusSettings.healingSalveLifetime);

            count --;
            yield return new WaitForSeconds(period);
        }       
    }
}
