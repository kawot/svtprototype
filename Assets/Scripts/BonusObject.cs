﻿using UnityEngine;
using System.Collections;

public class BonusObject : MonoBehaviour
{
    public float rotateSpeed = 1000f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.back * rotateSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameController.instance.ActivateBonus(collision.transform.position.y < 0f);
    }


}
