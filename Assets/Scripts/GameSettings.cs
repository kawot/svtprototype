﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "GameConfig", menuName = "Settings/Main", order = 1)]
public class GameSettings : ScriptableObject
{
    [Header("Game")]
    public int startTimer = 3;
    public float borderSize = 0.5f;
    [Space(10)]
    [Header("Sphere")]
    public float sphereSize = 1f;
    public float sphereMinSize = 0.1f;
    [Range(0f, 1f)] public float sphereSizeReduction = 0.02f;
    [Range(0f, 1f)] public float sphereSizeRestore = 0.1f;
    public float maxVelocity = 8f;
    [Space(10)]
    [Header("Missile")]
    public float missileSize = 0.2f;
    public float missileVelocity = 5f;
    public float missileFrequency = 5f;
    public float missileAngle = 60f;
    public float bulletLoadFrequency = 5f;
    public int bulletCapacity = 10;
    public int startingBullets = 7;
    [Space(10)]
    [Header("Base")]
    public float baseHight = 1f;
    public int lives = 25;
    [Space(10)]
    [Header("Puck")]
    public float puckSize = 1f;
    [Range(0f, 1f)] public float puckBounciness = 1f;
    public float puckDeceleration = 1f;
    public int puckDamage = 5;
    public float puckDelay = 5f;
    public float puckRespawnDelay = 2f;
}
